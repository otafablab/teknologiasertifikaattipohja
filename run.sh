#!/usr/bin/env sh

cargo watch -w "$1" -x "run '$1' > content/combined.txt"
