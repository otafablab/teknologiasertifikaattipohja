#!/usr/bin/env fish

set -l rivejä (sed -n '$=' $argv[1])
set -l maksimi_indentointi 15

for r in (seq 0 $rivejä)
    set -l i (random 0 $maksimi_indentointi)
    set -l rivi (head "-$r" $argv[1]| tail -1)
    for _i in (seq $i)
        printf ' '
    end
    echo $rivi
end