# Teknologiasertifikaattipohja

## TOML-konfiguraatio

TOML-tiedostolla kerrotaan kuinka ASCII tekstitiedostot yhistetään. Tiedoston formaatti on seuraava:

```toml
[[kuva]]
polku = "./donut1.txt"
sijainti = [50, 100]
reunaerotus = 3
```

Taulu `kuva` sisältää listan kuvia:

- `polku` tarkoittaa polkua ASCII tiedostoon
- `sijainti` tarkoittaa mihin x, y koordinaattiin kuvan vasen yläkulma tulee
- `reunaerotus` tarkoittaa kuinka ison säteen välilyöntierotus lisätään kuvan ulkoreunaan. 0 tarkoittaa pois päältä

Jokainen kuva liitetään lopputulokseen siinä järjestyksessä missä se on konfiguraatiotiedostossa.

## Käyttö

```shell
cargo run -- pohja.toml
```

Tulostaa standardiulostuloon yhistetyn kuvan

## Sertifikaatti prosessi

### Tekstit

- Luo sertifikaatin tekstit tekstinkäsittelyohjelmalla
- Lataa tekstit PDF-muodossa
- Avaa PDF inkscapella
- Jos otsikot ja tekstit eivät ole sopivalla tai samalla korkeudella toisiinsa nähden
  - Ryhmittele jokaisen sertifikaatin otsikko, tekstit, allekirjoituskentät yms
  - Valitse jokaisen sivun sama ryhmä
  - Avaa "Align and Distribute" välilehti
  - Klikkaa "Align top edges"
  - Toista kaikille elementeille
- Vie/tallenna PDF
- Avaa PDF inkscapessa ja poista jokaisen sivun valkoinen taustaelementti

### Taustat

- Asenna Rust
- Käynnistä cargo watch
  ```shell
  ./run.sh pohja.toml
  ```
- Käynnistä Python webipalvelin
  ```
  python -m http.server 1337
  ```
- Avaa `content/index.html` sivu (<http://localhost:1337/content/>)
- Muokkaa pohjatiedostoa `pohja.toml` ja uudelleenlataa selainta
- Kun olet tyytyväinen taustaan, vie PDF (Chromium on suositeltu tähän)
  - Vain ensimmäinen sivu
  - **A4** koossa
  - **Taustat enabloituna** (background graphics)
  - Margins Custom: 0.25" vasen ja oikea
  - Ilman ylimääräisiä merkintöjä

### Yhdistäminen

- Asenna `pdftk`
- Teksti PDF:n voi rikkoa omiksi sivuiksi käyttäen
  ```
  pdftk tekstit.pdf burst
  ```
- Muodosta yksittäisestä sivusta yhdistetty PDF käyttäen
  ```
  pdftk pg_0001.pdf multibakground tausta.pdf output output.pdf
  ```
  > `multibackground` latoo taustat sivu kerrallaan, joten voit myös yhdistää jo-yhdistetyt tekstit ja taustat komennolla
  ```
  pdftk tekstit.pdf multibakground taustat.pdf output "TEC-sertifikaatit kevät 0000 valmistuvat.pdf"
  ```
