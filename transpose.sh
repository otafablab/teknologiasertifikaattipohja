#!/usr/bin/env bash
delimiter=""
sep=""

awk ' 
{ for(i=1;i<=NF;i++){a[NR,i]=$i};{(NF>m)?m=NF:0} }
END { for(j=1; j<=m; j++)
      { for(i=1; i<=NR; i++)
        { b=((a[i,j]=="")?" ":a[i,j])
          printf("%s%s",(i==1)?"":sep,b)
        }
        printf("\n")
      }
    }
' FS="$delimiter" sep="$sep" cc="$countcols" $@
