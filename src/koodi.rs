use std::{borrow::Cow, fmt, fs, io, path::PathBuf};

use serde::*;

// Tärkeät tyyppimäärittelyt
type Merkkijono = String;
type MerkkijonoViipaleReferenssi<'a> = &'a str;
type MerkitönKoko = usize;
type PolkuPuskuri = PathBuf;
type Totuusarvo = bool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ASCIITaideTeosPolulla {
    pub polku: PolkuPuskuri,
    #[serde(flatten)]
    pub taideteos: ASCIITaideTeos,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ASCIITaideTeos {
    pub sijainti: [MerkitönKoko; 2],
    #[serde(default)]
    pub leveys: Option<MerkitönKoko>,
    #[serde(rename = "lisavali", default)]
    pub lisäväli: MerkitönKoko,
    #[serde(default)]
    pub reunaerotus: MerkitönKoko,
    #[serde(rename = "kaaria", default)]
    pub kääriä: Totuusarvo,
    #[serde(rename = "rikkovavalilyonti", default)]
    pub rikkovavälilyönti: Totuusarvo,
}

impl ASCIITaideTeos {
    /// Megacursed funktio
    pub fn formatoi(&self, texti: &str, f: &mut impl io::Write) -> io::Result<()> {
        let mut nykyinen_leveys = 0;
        let leveys = self.leveys.unwrap_or(MerkitönKoko::MAX);
        if self.kääriä {
            for sana in texti.split_ascii_whitespace() {
                if sana.chars().count() + nykyinen_leveys > leveys {
                    for _ in 0..=self.lisäväli {
                        if self.rikkovavälilyönti {
                            for _ in 0..leveys.saturating_sub(nykyinen_leveys) {
                                write!(f, " ")?; // ei-rikkova välilyönti
                            }
                        }
                        writeln!(f)?;
                        nykyinen_leveys = 0;
                    }
                }
                write!(f, "{sana} ")?;
                nykyinen_leveys += sana.len() + 1;
            }
            if self.rikkovavälilyönti {
                for _ in 0..leveys.saturating_sub(nykyinen_leveys) {
                    write!(f, " ")?; // ei-rikkova välilyönti
                }
            }
        } else {
            for rivi in texti.split('\n') {
                let rivi = if self.rikkovavälilyönti {
                    Cow::Owned(rivi.replace(" ", " "))
                } else {
                    Cow::Borrowed(rivi)
                };
                write!(f, "{rivi}")?;
                if self.rikkovavälilyönti {
                    for _ in 0..leveys.saturating_sub(rivi.chars().count()) {
                        write!(f, " ")?; // ei-rikkova välilyönti
                    }
                }
                writeln!(f)?;
                for _ in 0..self.lisäväli {
                    if self.rikkovavälilyönti {
                        for _ in 0..leveys {
                            write!(f, " ")?; // ei-rikkova välilyönti
                        }
                    }
                    writeln!(f)?;
                }
            }
        }

        Ok(())
    }
}

impl ASCIITaideTeos {
    fn uusi(
        sijainti: [MerkitönKoko; 2],
        leveys: Option<MerkitönKoko>,
        lisäväli: MerkitönKoko,
        reunaerotus: MerkitönKoko,
        kääriä: Totuusarvo,
        rikkovavälilyönti: Totuusarvo,
    ) -> Self {
        Self {
            sijainti,
            leveys,
            lisäväli,
            reunaerotus,
            kääriä,
            rikkovavälilyönti,
        }
    }
}

#[cfg(test)]
mod testit {
    use super::*;

    #[test]
    fn koodi_toimii_oikein() {
        let k = ASCIITaideTeos::uusi([999, 999], Some(5), 1, 99999, true, true);
        let texti = r#"fn pääfunktio() { println("Hei maailma!"); }"#;
        let mut formatoitu = Vec::new();
        k.formatoi(texti, &mut formatoitu);
        assert_eq!(
            std::str::from_utf8(&formatoitu)
                .unwrap()
                .split('\n')
                .map(|sp| dbg!(sp.chars()).count())
                .collect::<Vec<_>>(),
            &[5, 5, 13, 5, 5, 5, 13, 5, 12, 5, 5]
        );
    }
}
