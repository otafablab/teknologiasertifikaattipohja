use std::ops::{Add, Mul};

pub fn konvoluutio2d<C>(
    mut a: impl ExactSizeIterator<Item = impl ExactSizeIterator<Item = C>>,
    b: impl Iterator<Item = impl Iterator<Item = C>>,
) -> Vec<Vec<C>>
where
    C: Mul<Output = C>,
    C: Add<Output = C>,
    C: Default + Copy,
{
    let ah = a.len();
    let a1 = if let Some(a1) = a.next() {
        a1
    } else {
        return vec![];
    };
    let aw = a1.len();

    let b = b.map(|i| i.collect::<Vec<_>>()).collect::<Vec<_>>();
    let bh = b.len();
    if bh == 0 {
        return vec![];
    };
    let bw = b[0].len();

    let mut o = (0..ah + bh - 1)
        .map(|_| (0..aw + bw - 1).map(|_| C::default()).collect::<Vec<_>>())
        .collect::<Vec<_>>();

    for (arow, avals) in std::iter::once(a1).chain(a).enumerate() {
        for (acol, aval) in avals.enumerate() {
            for (browr, bvals) in b.iter().rev().enumerate() {
                for (bcolr, bval) in bvals.iter().rev().enumerate() {
                    let orow = arow + browr;
                    let ocol = acol + bcolr;
                    o[orow][ocol] = o[orow][ocol] + aval * *bval;
                }
            }
        }
    }
    o
}

pub fn maskita(s: &str) -> Vec<Vec<Binäärinumero>> {
    let mut o: Vec<Vec<Binäärinumero>> = vec![];

    let w = s.split('\n').map(|s| s.len()).max().unwrap();

    for (rivi, merkit) in s.split('\n').map(|rivi| rivi.chars()).enumerate() {
        o.push(
            std::iter::repeat(Binäärinumero::default())
                .take(w)
                .collect(),
        );

        for (sarake, m) in merkit.enumerate() {
            match m {
                crate::VÄLIMERKKI => {}
                _ => o[rivi][sarake] = Binäärinumero::T,
            };
        }
    }
    o
}

pub fn reunaerotusydin(säde: usize) -> Vec<Vec<Binäärinumero>> {
    let mut ydin: Vec<Vec<_>> = std::iter::repeat(
        std::iter::repeat(Binäärinumero::default())
            .take(säde * 2 + 1)
            .collect(),
    )
    .take(säde * 2 + 1)
    .collect();
    for (i, rivi) in ydin.iter_mut().enumerate() {
        for (j, alkio) in rivi.iter_mut().enumerate() {
            let etäisyys = (i as i32 - säde as i32).pow(2) + (j as i32 - säde as i32).pow(2);
            if etäisyys < (säde as i32).pow(2) + 1 {
                *alkio = Binäärinumero::T
            }
        }
    }
    ydin
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Binäärinumero {
    T,
    F,
}

impl Add for Binäärinumero {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        use Binäärinumero::*;
        match (self, rhs) {
            (F, F) => F,
            _ => T,
        }
    }
}

impl Mul for Binäärinumero {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        use Binäärinumero::*;
        match (self, rhs) {
            (T, T) => T,
            _ => F,
        }
    }
}

impl Default for Binäärinumero {
    fn default() -> Self {
        Self::F
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const T: Binäärinumero = Binäärinumero::T;
    const F: Binäärinumero = Binäärinumero::F;

    fn slais_intoiter<T: Clone>(
        slais: &[&[T]],
    ) -> impl ExactSizeIterator<Item = impl ExactSizeIterator<Item = T>> {
        let mut o = vec![];
        for &slais in slais.iter() {
            o.push(slais.iter().cloned().collect::<Vec<_>>().into_iter());
        }
        o.into_iter()
    }

    #[test]
    fn binäärimatriisi1() {
        let a: &[&[Binäärinumero]] = &[&[T]];
        let b: &[&[Binäärinumero]] = &[&[T, F, T], &[F, T, F], &[T, F, T]];

        let konvoluutio = konvoluutio2d(slais_intoiter(a), slais_intoiter(b));
        assert_eq!(konvoluutio, b);
    }

    #[test]
    fn binäärimatriisi2() {
        let a: &[&[Binäärinumero]] = &[&[F, F, F], &[F, T, F], &[F, F, F]];
        let b: &[&[Binäärinumero]] = &[&[T, F, F], &[F, T, F], &[F, F, F]];

        let konvoluutio = konvoluutio2d(slais_intoiter(a), slais_intoiter(b));
        assert_eq!(
            konvoluutio,
            &[
                &[F, F, F, F, F],
                &[F, F, F, F, F],
                &[F, F, T, F, F],
                &[F, F, F, T, F],
                &[F, F, F, F, F]
            ]
        );
    }

    #[test]
    fn binäärimatriisi3() {
        let a: &[&[Binäärinumero]] = &[&[F, T, F], &[F, T, F], &[F, F, T]];
        let b: &[&[Binäärinumero]] = &[&[F, T, F], &[T, T, T], &[F, T, F]];

        let konvoluutio = konvoluutio2d(slais_intoiter(a), slais_intoiter(b));
        assert_eq!(
            konvoluutio,
            &[
                &[F, F, T, F, F],
                &[F, T, T, T, F],
                &[F, T, T, T, F],
                &[F, F, T, T, T],
                &[F, F, F, T, F]
            ]
        );
    }

    #[test]
    fn maskita_kuva() {
        let kuva = " k\nana\n ";

        assert_eq!(maskita(kuva), &[&[F, T, F], &[T, T, T], &[F, F, F]]);
    }

    #[test]
    fn reunaerotusydin2() {
        assert_eq!(
            reunaerotusydin(2),
            &[
                &[F, F, T, F, F],
                &[F, T, T, T, F],
                &[T, T, T, T, T],
                &[F, T, T, T, F],
                &[F, F, T, F, F]
            ]
        );
    }
}
