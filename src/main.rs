use konvoluutio::Binäärinumero;
use koodi::ASCIITaideTeosPolulla;
use serde::{Deserialize, Serialize};
use std::{
    fmt::{Display, Write},
    fs::{self, read_to_string},
    io,
    path::PathBuf,
};

use crate::konvoluutio::{konvoluutio2d, maskita, reunaerotusydin};

mod konvoluutio;
mod koodi;

const VÄLIMERKKI: char = ' ';

#[derive(Debug, Default, Clone)]
struct Kanvas {
    rivit: Vec<Vec<char>>,
}

impl Kanvas {
    pub fn tyhjä() -> Self {
        Self::default()
    }

    pub fn liitä_merkit_kohtaan(
        &mut self,
        merkit: impl Iterator<Item = impl Iterator<Item = char>>,
        rivi: usize,
        sarake: usize,
    ) {
        for (r, merkit) in merkit.enumerate() {
            for (s, m) in merkit.enumerate() {
                self.liitä_merkki(m, rivi + r, sarake + s);
            }
        }
    }

    pub fn liitä_merkki(&mut self, m: char, rivi: usize, sarake: usize) {
        while rivi >= self.rivit.len() {
            self.rivit.push(Vec::new());
        }

        while sarake >= self.rivit[rivi].len() {
            self.rivit[rivi].push(VÄLIMERKKI);
        }

        match m {
            VÄLIMERKKI => {}
            m => self.rivit[rivi][sarake] = m,
        };
    }

    pub fn tyhjennä_maski_kohdasta(
        &mut self,
        maski: impl Iterator<Item = impl Iterator<Item = Binäärinumero>>,
        rivi: usize,
        sarake: usize,
    ) {
        for (r, maski) in maski.enumerate() {
            for (s, m) in maski.enumerate() {
                match m {
                    Binäärinumero::F => {}
                    _ => {
                        if let Some(rivi) = self.rivit.get_mut(rivi + r) {
                            if let Some(m) = rivi.get_mut(sarake + s) {
                                *m = VÄLIMERKKI;
                            }
                        }
                    }
                }
            }
        }
    }
}

impl Display for Kanvas {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for merkkijono in &self.rivit {
            for m in merkkijono {
                f.write_char(*m)?;
            }
            f.write_char('\n')?;
        }
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Konfiguraatio {
    taideteos: Vec<ASCIITaideTeosPolulla>,
}

/// Pääfunktio main, kutsutaan libc _start funktiosta
fn main() -> io::Result<()> {
    let toml_path = PathBuf::from(
        std::env::args()
            .nth(1)
            .expect("spesifioi polku TOML-konfiguraatiotiedostoon"),
    );
    let konfiguraatiot: Konfiguraatio =
        toml::from_str(&read_to_string(toml_path).expect("ei pystytty lukemaan konfiguraatiota"))
            .expect("konfiguraatiotiedostossa on roskaa");

    let mut kanvas = Kanvas::tyhjä();
    for ASCIITaideTeosPolulla { polku, taideteos } in konfiguraatiot.taideteos {
        let sisältö = fs::read_to_string(polku).expect("taideteosta ei löytynyt");
        let mut puskuri = Vec::new();
        taideteos.formatoi(&sisältö, &mut puskuri)?;
        let sisältö = String::from_utf8_lossy(&puskuri);

        if taideteos.reunaerotus != 0 {
            let maski = maskita(&sisältö);
            let konvoluutio = konvoluutio2d::<Binäärinumero>(
                maski.into_iter().map(Vec::into_iter),
                reunaerotusydin(taideteos.reunaerotus)
                    .into_iter()
                    .map(Vec::into_iter),
            );
            let maski = konvoluutio.into_iter().map(Vec::into_iter);
            kanvas.tyhjennä_maski_kohdasta(
                maski,
                taideteos.sijainti[1]
                    .checked_sub(taideteos.reunaerotus)
                    .expect("reunaerotus ei saa ulottua kuvan ulkopuolelle"),
                taideteos.sijainti[0]
                    .checked_sub(taideteos.reunaerotus)
                    .expect("reunaerotus ei saa ulottua kuvan ulkopuolelle"),
            )
        }
        // TODO korjaa mega kopybeistit
        kanvas.liitä_merkit_kohtaan(
            sisältö.split('\n').map(|rivi| rivi.chars()),
            taideteos.sijainti[1],
            taideteos.sijainti[0],
        );
    }
    println!("{kanvas}");
    Ok(())
}

#[cfg(test)]
mod testit {
    use crate::konvoluutio::maskita;

    use super::*;

    #[test]
    fn liitä_merkki() {
        let mut kanvas = Kanvas::tyhjä();

        kanvas.liitä_merkki('k', 2, 1);
        kanvas.liitä_merkki('a', 2, 2);
        kanvas.liitä_merkki('n', 2, 3);
        kanvas.liitä_merkki('a', 3, 3);

        assert_eq!(format!("{}", kanvas), "\n\n kan\n   a\n");
    }

    #[test]
    fn liitä_välimerkki() {
        let mut kanvas = Kanvas::tyhjä();

        kanvas.liitä_merkki('k', 1, 1);
        kanvas.liitä_merkki(' ', 1, 1);

        assert_eq!(format!("{}", kanvas), "\n k\n");
    }

    #[test]
    fn liitä_merkit_kohtaan() {
        let mut kanvas = Kanvas::tyhjä();

        let merkit = vec!["kbna".chars(), " lihakeitto".chars()];
        kanvas.liitä_merkit_kohtaan(merkit.into_iter(), 0, 0);
        assert_eq!(format!("{}", kanvas), "kbna\n lihakeitto\n");

        let merkit = vec!["kan p".chars(), "kukko".chars()];
        kanvas.liitä_merkit_kohtaan(merkit.into_iter(), 1, 1);
        assert_eq!(format!("{}", kanvas), "kbna\n kanapeitto\n kukko\n");
    }

    #[test]
    fn tyhjennä_maski_kohdasta() {
        let mut kanvas = Kanvas::tyhjä();

        let merkit = vec![
            "#####".chars(),
            "#####".chars(),
            "#####".chars(),
            "#####".chars(),
            "#####".chars(),
        ];
        kanvas.liitä_merkit_kohtaan(merkit.into_iter(), 0, 0);

        let maski = maskita(" .\n. ..\n . .\n");
        let maski2 = maski.clone().into_iter().map(Vec::into_iter);
        kanvas.tyhjennä_maski_kohdasta(maski2, 1, 2);
        assert_eq!(format!("{}", kanvas), "#####\n### #\n## # \n### #\n#####\n");

        let maski2 = maski.into_iter().map(Vec::into_iter);
        kanvas.tyhjennä_maski_kohdasta(maski2, 0, 0);
        assert_eq!(format!("{}", kanvas), "# ###\n #  #\n#    \n### #\n#####\n");
    }
}
